PHP REST API
=============================

This is android application for prototype of chat apps

## Building the Sample 

First, clone the repo:

`git clone https://gitlab.com/pratamanidhi/rest-api-development.git`

Building the sample then depends on your build tools.

## Running the sample project

Running your local database and your local server (Recomendation : Xampp)
Copy the clone project to your Xampp folder and pasted it to htdocs

* Run the service using Postman
* command `localhost/rest-api-development/user/user.php`


